package com.foosball;

import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.nio.file.Paths;
import java.util.Random;

public class FoosballGame {
    public static final String SOUNDBANK_PATH = "Sounds";
    private static final float INSULT_RATE = 0.25f;
    private IRaspberryPiMediator rpiView;
    private String teamOneName;
    private String teamTwoName;
    private AudioFileModel audioFileModel;
    private AudioFilePlayer audioFilePlayer;
    private int teamOneTally;
    private int teamOneRoundsWon;
    private int teamTwoTally;
    private int teamTwoRoundsWon;
    private Pin TEAM_ONE_PIN = RaspiPin.GPIO_02;
    private Pin TEAM_TWO_PIN = RaspiPin.GPIO_03;

    private static final boolean USE_INSULTS = false;

    public FoosballGame(String soundPackTeam1, String soundPackTeam2, AudioFileModel audioFileModel, AudioFilePlayer audioFilePlayer, IRaspberryPiMediator rpiMediator) {
        System.out.println("Match: " + soundPackTeam1 + " VS. " + soundPackTeam2);
        this.teamOneName = soundPackTeam1;
        this.teamTwoName = soundPackTeam2;
        this.audioFileModel = audioFileModel;
        this.audioFilePlayer = audioFilePlayer;
        this.rpiView = rpiMediator;
        resetTallies();
        teamOneRoundsWon = 0;
        teamTwoRoundsWon = 0;
    }

    public void startGame() {
        this.audioFileModel.load(1, Paths.get(SOUNDBANK_PATH, teamOneName).toFile());
        this.audioFileModel.load(2, Paths.get(SOUNDBANK_PATH, teamTwoName).toFile());


        if(rpiView.getTeamOneSensor() != null)
        rpiView.getTeamOneSensor().addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
                doActionForTeamWithPin(event.getPin());
            }

        });

        if(rpiView.getTeamTwoSensor() != null)
        rpiView.getTeamTwoSensor().addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
                doActionForTeamWithPin(event.getPin());
            }

        });
    }

    private void doActionForTeamWithPin(GpioPin pin) {

            if(pin == TEAM_ONE_PIN)
                scoreTeamOne();
            else if (pin == TEAM_TWO_PIN)
                scoreTeamTwo();
    }

    private void insultFromTeamTwo() {
        this.audioFilePlayer.playInsultFromTeamTwo();
    }

    private void insultFromTeamOne() {
        this.audioFilePlayer.playInsultFromTeamOne();
    }

    public void scoreTeamOne() {

        Random insultRandom = new Random();
        if(insultRandom.nextFloat() < INSULT_RATE && USE_INSULTS)
            insultFromTeamOne();
        else
            this.audioFilePlayer.playSoundForTeamOne();
        
        teamOneTally++;

        evaluateGameState();
    }

    private void evaluateGameState() {
        if(!gameOver() && roundOver())
            switchTeams();
        else
            endGame();
    }

    private void switchTeams() {
        updateRoundsWon();
        resetTallies();
        switchPins();
    }

    private void updateRoundsWon() {
        if(teamOneTally > teamTwoTally)
            teamOneRoundsWon++;
        else
            teamTwoRoundsWon++;
    }

    private void resetTallies() {
        teamOneTally = 0;
        teamTwoTally = 0;
    }

    private void switchPins() {
        Pin tempPin = TEAM_ONE_PIN;
        TEAM_ONE_PIN = TEAM_TWO_PIN;
        TEAM_TWO_PIN = tempPin;
    }

    private void endGame() {
        rpiView.terminate();
    }

    private boolean roundOver() {
        return (teamOneTally == 10 || teamTwoTally == 10);
    }

    private boolean gameOver() {
        return (teamOneRoundsWon - teamTwoRoundsWon >= 2 || teamTwoRoundsWon - teamOneRoundsWon >= 2);
    }

    public void scoreTeamTwo() {
        Random insultRandom = new Random();
        if(insultRandom.nextFloat() < INSULT_RATE && USE_INSULTS)
            insultFromTeamTwo();
        else
            this.audioFilePlayer.playSoundForTeamTwo();

        teamTwoTally++;

        evaluateGameState();
    }
}
