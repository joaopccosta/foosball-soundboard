package com.foosball;

import com.pi4j.io.gpio.GpioPinDigitalInput;

public class FakeRaspberryPiMediator implements IRaspberryPiMediator {

    public GpioPinDigitalInput getTeamOneSensor() {
        return null;
    }

    public GpioPinDigitalInput getTeamTwoSensor() {
        return null;
    }

    public void terminate() {

    }
}
