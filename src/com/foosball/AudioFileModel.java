package com.foosball;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class AudioFileModel {

    private static final String AUDIO_FILE_EXTENSION = "wav";
    private ArrayList<File> teamOneSoundList;
    private ArrayList<File> teamTwoSoundList;
    private Random random;

    public AudioFileModel() {
        this.teamOneSoundList = new ArrayList<File>();
        this.teamTwoSoundList = new ArrayList<File>();
        random = new Random();
    }

    public void load(int teamNumber, File parentFile)
    {
        for(File file : parentFile.listFiles())
        {
            if(FilenameUtils.getExtension(file.getName()).equals(AUDIO_FILE_EXTENSION))
            {
                if(teamNumber == 1)
                {
                    teamOneSoundList.add(file);
                }
                else
                {
                    teamTwoSoundList.add(file);
                }
            }
        }

    }

    public ArrayList<File> teamOneSounds() {
        return teamOneSoundList;
    }

    public ArrayList<File> teamTwoSounds() {
        return teamTwoSoundList;
    }

    private File getSoundFromList(ArrayList<File> list)
    {
        int bounds = teamOneSoundList.size();
        int index = random.nextInt(bounds);
        System.out.println("Getting "+index+" out of "+bounds);
        return list.get(index);
    }

    public File getSoundForTeamOne() {
        return getSoundFromList(teamOneSoundList);
    }

    public File getSoundForTeamTwo() {
        return getSoundFromList(teamTwoSoundList);
    }

    public File getInsultSoundForTeamOne() {
        try {
            throw new Exception("NOT IMPLMENTED YET!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public File getInsultSoundForTeamTwo() {
        try {
            throw new Exception("NOT IMPLMENTED YET!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
