package com.foosball;

import com.pi4j.io.gpio.GpioPinDigitalInput;

public interface IRaspberryPiMediator {
    GpioPinDigitalInput getTeamOneSensor();

    GpioPinDigitalInput getTeamTwoSensor();

    void terminate();
}
