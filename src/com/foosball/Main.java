package com.foosball;

import java.io.File;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        if(args[0].equals("list")) {
            System.out.println("Available teams:");
            System.out.println(listTeams());
            return;
        }

        String soundPackTeam1 = args[0];
        String soundPackTeam2 = args[1];
        AudioFileModel audioFileModel = new AudioFileModel();
        AudioFilePlayer audioFilePlayer = new AudioFilePlayer(audioFileModel);
//        IRaspberryPiMediator rpiMediator = new RaspberryPiMediator();
        IRaspberryPiMediator rpiMediator = new FakeRaspberryPiMediator();
        FoosballGame foossballGame = new FoosballGame(soundPackTeam1, soundPackTeam2, audioFileModel, audioFilePlayer, rpiMediator);

        foossballGame.startGame();


        Thread.sleep(4000);
        foossballGame.scoreTeamOne();
        Thread.sleep(4000);
        foossballGame.scoreTeamTwo();

        System.out.println("Game Over");
    }

    private static String listTeams() {
        StringBuilder stringbuilder = new StringBuilder();
        File soundFolder = new File(FoosballGame.SOUNDBANK_PATH);
        for( File teamFolder : soundFolder.listFiles())
        {
            stringbuilder.append(" - "+teamFolder.getName() + "("+ teamFolder.listFiles().length+")"+ "\n");
        }
        return stringbuilder.toString();
    }
}
