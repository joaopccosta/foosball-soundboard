package com.foosball;

import com.pi4j.io.gpio.*;

public class RaspberryPiMediator implements IRaspberryPiMediator
{
    private final GpioPinDigitalInput teamOneSensor;
    private final GpioPinDigitalInput teamTwoSensor;
    private boolean terminate;

    public RaspberryPiMediator() {

        final GpioController gpio = GpioFactory.getInstance();

        teamOneSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN);
        teamTwoSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_DOWN);
        teamOneSensor.setShutdownOptions(true);
        teamTwoSensor.setShutdownOptions(true);
        terminate = false;
        System.out.println("GPIO #02 and #03 circuit and see the listener feedback here in the console.");

        start();

        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
         gpio.shutdown();//   <--- implement this method call if you wish to terminate the Pi4J GPIO controller
    }

    private void start() {
        while(!terminate) {
            try {
                System.out.println(" - waiting for piezo activity...");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public GpioPinDigitalInput getTeamOneSensor() {
        return teamOneSensor;
    }

    public GpioPinDigitalInput getTeamTwoSensor() {
        return teamTwoSensor;
    }

    public void terminate() {
        this.terminate = true;
    }
}
