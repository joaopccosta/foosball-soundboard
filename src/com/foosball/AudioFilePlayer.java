package com.foosball;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class AudioFilePlayer implements LineListener{

    private boolean playCompleted = false;
    private AudioFileModel audioFileModel;

    public AudioFilePlayer(AudioFileModel audioFileModel) {
        this.audioFileModel = audioFileModel;
    }

    public void playSoundForTeamOne()
    {
        File sound = this.audioFileModel.getSoundForTeamOne();
        playSound(sound);
    }

    public void playSoundForTeamTwo() {
        File sound = this.audioFileModel.getSoundForTeamTwo();
        playSound(sound);
    }

    private void playSound(File sound) {
        System.out.println("PLAYING: " +sound.getAbsolutePath() );
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(sound);
            Clip clip = AudioSystem.getClip();

            clip.addLineListener(this);


            clip.open(audioInputStream);
            clip.start();

            while (!playCompleted) {
                // wait for the playback completes
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            clip.close();
            playCompleted = false;


        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
            System.out.println("Playback started.");

        } else if (type == LineEvent.Type.STOP) {
            playCompleted = true;
            System.out.println("Playback completed.");
        }

    }

    public void playInsultFromTeamTwo() {
        File sound = this.audioFileModel.getInsultSoundForTeamOne();
        playSound(sound);
    }

    public void playInsultFromTeamOne() {
        File sound = this.audioFileModel.getInsultSoundForTeamTwo();
        playSound(sound);
    }
}
