package com.foosball;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.io.File;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FoosballGameTest {
    public static final String MOCK_PATH_1 = "FAKE_TEAM_1_PATH";
    public static final String MOCK_PATH_2 = "FAKE_TEAM_2_PATH";
    private FoosballGame foosballGame;
    private AudioFileModel audioFileModel;
    private AudioFilePlayer audioFilePlayer;
    private RaspberryPiMediator rpiMediator;

    @Before
    public void setUp() {
        audioFileModel = mock(AudioFileModel.class);
        audioFilePlayer = mock(AudioFilePlayer.class);
        rpiMediator = mock(RaspberryPiMediator.class);
        when(rpiMediator.getTeamOneSensor()).thenReturn(mock(GpioPinDigitalInput.class));
        when(rpiMediator.getTeamTwoSensor()).thenReturn(mock(GpioPinDigitalInput.class));
        foosballGame = new FoosballGame(MOCK_PATH_1, MOCK_PATH_2, audioFileModel, audioFilePlayer, rpiMediator);
    }

    @Test
    public void whenStartGame_loadsAllSoundsForBothTeams()
    {
        foosballGame.startGame();
        verify(audioFileModel, Mockito.times(2)).load(anyInt(), any(File.class));
    }

    @Test
    public void onTeamOneGoal_playsTeamOneSound()
    {
        foosballGame.scoreTeamOne();
        verify(audioFilePlayer).playSoundForTeamOne();
    }

    @Test
    public void onTeamTwoGoal_playsTeamTwoSound()
    {
        foosballGame.scoreTeamTwo();
        verify(audioFilePlayer).playSoundForTeamTwo();
    }
}
