package com.foosball;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AudioFileModelTest {

    public static final int TEAM_ONE = 1;
    public static final String FAKE_SOUND_PATH = "resources/faketeam/";
    public static final int NUMBER_OF_FILES_IN_FAKE_SOUND_PATH = 3;

    private AudioFileModel audioFileModel;
    private File mockAudioFilesFolder;

    @Before
    public void setUp() {
        mockAudioFiles();

        audioFileModel = new AudioFileModel();
    }

    @Test
    public void whenLoad_loadsAllAudioFilesForTheGivenTeamOntoADictionary()
    {
        audioFileModel.load(TEAM_ONE, mockAudioFilesFolder);
        Assert.assertEquals(2, audioFileModel.teamOneSounds().size());
    }

    private File mockAudioFiles() {
        mockAudioFilesFolder = mock(File.class);
        File[] mockListOfAudioFiles = new File[NUMBER_OF_FILES_IN_FAKE_SOUND_PATH];
        when(mockAudioFilesFolder.listFiles()).thenReturn(mockListOfAudioFiles);
        mockListOfAudioFiles[0] = mock(File.class);
        mockListOfAudioFiles[1] = mock(File.class);
        mockListOfAudioFiles[2] = mock(File.class);
        when(mockListOfAudioFiles[0].getName()).thenReturn("an_audio_file.wav");
        when(mockListOfAudioFiles[1].getName()).thenReturn("not_an_audio_file.txt");
        when(mockListOfAudioFiles[2].getName()).thenReturn("another_audio_file.wav");
        return mockAudioFilesFolder;
    }

}
